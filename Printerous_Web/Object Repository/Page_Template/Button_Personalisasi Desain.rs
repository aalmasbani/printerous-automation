<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_Personalisasi Desain</name>
   <tag></tag>
   <elementGuidId>c998fe49-ae73-42c3-bf48-c0af7912dcd5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[9]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.text-right.mt-4 > button.btn.btn-primary</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>972eb3f8-42a8-4caf-bf7c-a52c22148d84</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>fda31685-e8c7-425a-bf74-bb382fcacf1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>567d56e7-6afd-4665-adc6-253334ece9f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Personalisasi Desain</value>
      <webElementGuid>27d5a42e-3aec-4ab7-a686-c68db1868da8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;pb-4 modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-8&quot;]/div[@class=&quot;text-right mt-4&quot;]/button[@class=&quot;btn btn-primary&quot;]</value>
      <webElementGuid>ae0739d3-2850-4987-99a5-a8ff6e878511</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[9]</value>
      <webElementGuid>695c11da-8509-4406-b4fc-955fde6be925</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hitung'])[1]/following::button[1]</value>
      <webElementGuid>c232540f-c04b-4a46-9ba2-02450ac62e6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='pc'])[1]/following::button[2]</value>
      <webElementGuid>6d069e92-9d02-4874-bb15-368e4c130a01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Personalisasi Desain']/parent::*</value>
      <webElementGuid>f21c8a2b-28a8-4d23-b1cd-32e7fea66803</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/button</value>
      <webElementGuid>9f6a469a-13a6-4d87-bd88-f94bad1a6036</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Personalisasi Desain' or . = 'Personalisasi Desain')]</value>
      <webElementGuid>ac79a21a-d72a-4e3e-ad4f-6cf58bf192fd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
