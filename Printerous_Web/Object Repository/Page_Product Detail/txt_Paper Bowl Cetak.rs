<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_Paper Bowl Cetak</name>
   <tag></tag>
   <elementGuidId>59df28e9-696e-4799-b7da-01d39f70c9cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/section/div/div[2]/div[2]/h1</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h1.med-24.text-n-90.mb-3</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;root&quot;)/section[@class=&quot;css-1jomrwc&quot;]/div[@class=&quot;pb-3 container&quot;]/div[@class=&quot;mt-4 row&quot;]/div[@class=&quot;col-md-8&quot;]/h1[@class=&quot;med-24 text-n-90 mb-3&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>8df31137-dbe2-4609-bca9-31e66950a296</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>med-24 text-n-90 mb-3</value>
      <webElementGuid>0da1e961-4ee5-43c7-9a76-b22fe3a62882</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Paper Bowl Cetak</value>
      <webElementGuid>ca91bdb7-6c37-4cc4-9a39-c31278eacfd8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/section[@class=&quot;css-1jomrwc&quot;]/div[@class=&quot;pb-3 container&quot;]/div[@class=&quot;mt-4 row&quot;]/div[@class=&quot;col-md-8&quot;]/h1[@class=&quot;med-24 text-n-90 mb-3&quot;]</value>
      <webElementGuid>74af651c-4a12-465a-9bfc-f11a54d7ad7b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/section/div/div[2]/div[2]/h1</value>
      <webElementGuid>20721ab9-247d-4240-9583-af6ffd550ada</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mulai Product Tour'])[1]/following::h1[1]</value>
      <webElementGuid>725e9caf-4571-47a4-8c7e-e7501ed06204</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bisa dicetak dengan desain custom full color'])[1]/preceding::h1[1]</value>
      <webElementGuid>d3679479-10e2-4221-a10c-c322cb9f42fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>f27620b8-2de3-49ba-b358-5cb4ea6c5a37</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Paper Bowl Cetak' or . = 'Paper Bowl Cetak')]</value>
      <webElementGuid>0ac7e67c-45a0-405a-9e71-aa9c159ae848</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
