<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_Kategori Pilihan</name>
   <tag></tag>
   <elementGuidId>a35985d2-0cc4-4b02-982e-fca4d4e62351</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h2.mb-3.med-21.text-n-90</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/section[3]/div/h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>bc834952-ccbe-4b6c-88bd-5dad7f7b30d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mb-3 med-21 text-n-90</value>
      <webElementGuid>0fc1f14d-c07d-47bd-885e-b75f9cfab331</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Kategori Pilihan</value>
      <webElementGuid>7f2fff02-92d8-4f90-a639-17d7b1e4dfe9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/section[@class=&quot;py-5&quot;]/div[@class=&quot;container&quot;]/h2[@class=&quot;mb-3 med-21 text-n-90&quot;]</value>
      <webElementGuid>2218ae2f-fc1d-4cf3-b4cf-ee7ef70887d1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/section[3]/div/h2</value>
      <webElementGuid>9a37bf54-e50c-4cd4-8bab-77b8abaf8596</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dapatkan penawaran harga terbaik secara real-time.'])[1]/following::h2[1]</value>
      <webElementGuid>9a3f71f0-b9f3-40c2-8764-f5184c3fbe74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Murah Harganya'])[1]/following::h2[1]</value>
      <webElementGuid>7c4e3f72-770e-40a6-8cde-a07d0ca2292b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kemasan &amp; Label'])[2]/preceding::h2[1]</value>
      <webElementGuid>a52e57e3-6e2f-46ce-8b8d-73ff6b0ec3e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Perlengkapan Kantor'])[2]/preceding::h2[1]</value>
      <webElementGuid>3e615d75-02cd-47b1-95a8-9ad9d58e2b5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kategori Pilihan']/parent::*</value>
      <webElementGuid>a9919ef0-162c-4aa5-8c4a-b0b782d4b498</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>9ab75a35-8e41-4249-b8f9-e8763b6a131b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Kategori Pilihan' or . = 'Kategori Pilihan')]</value>
      <webElementGuid>b9fab84b-b725-4c36-aa9a-95df9bd82b48</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
