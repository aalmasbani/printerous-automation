<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Txt_Produk Populer</name>
   <tag></tag>
   <elementGuidId>1cce38bd-d5c1-4a85-9b5d-0a9a7279ee07</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/section[4]/div/h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>c78f0f93-e8d2-4f3e-8c1b-b59d1c18941a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mb-3 med-21 text-n-90</value>
      <webElementGuid>aa5c3fd7-d5ea-4d13-bba3-17e5f679482c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Produk Populer</value>
      <webElementGuid>736c1877-1c51-4870-b09d-0bcea5482806</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/section[@class=&quot;py-5&quot;]/div[@class=&quot;container&quot;]/h2[@class=&quot;mb-3 med-21 text-n-90&quot;]</value>
      <webElementGuid>8ef4492e-af7d-46b9-b846-c0221d841426</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/section[4]/div/h2</value>
      <webElementGuid>82526625-bcf0-4236-b512-d34ba825f84e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Normal'])[2]/following::h2[1]</value>
      <webElementGuid>e784cf7e-ad97-4cff-baa1-b0be48909546</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kebutuhan Marketing'])[2]/following::h2[1]</value>
      <webElementGuid>fd257fab-b591-42ac-b4cc-6ead0a025945</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Standing Pouch Aluminium'])[1]/preceding::h2[1]</value>
      <webElementGuid>2bf2dcf5-05b8-4e42-8f69-d61d02095661</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Produk Populer']/parent::*</value>
      <webElementGuid>ec4d115b-2471-4017-a8c7-b6f64425020a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[4]/div/h2</value>
      <webElementGuid>559d6db2-57b2-4217-b470-4c33172d2069</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Produk Populer' or . = 'Produk Populer')]</value>
      <webElementGuid>dc9e429a-79c4-4568-bd8d-ace5b086ddd8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
