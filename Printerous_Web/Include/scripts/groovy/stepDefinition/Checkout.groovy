package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


public class Checkout {
	@When("User click cart icon")
	public void user_click_cart_icon() {
		WebUI.click(findTestObject('Page_Homepage/Icon_Cart'))
	}

	@When("User select Jasa Pegiriman")
	public void user_select_jasa_pegiriman() {	
		WebUI.click(findTestObject('Page_Cart/Form Jasa Pengiriman'))
		WebUI.delay(5)
		WebUI.click(findTestObject('Page_Cart/Txt_Reguler (4 Hari)'))
	}

	@When("User click button Pembayaran")
	public void user_click_button_Pembayaran() {
		WebUI.click(findTestObject('Page_Cart/Button_Pilih Pembayaran'))
	}

	@When("User redirect to payment page")
	public void user_redirect_to_payment_page() {
		WebUI.verifyElementVisible(findTestObject('Page_Pembayaran/Txt_Pembayaran'))
	}

	@Then("User select Payment Method")
	public void user_Select_Payment_Method() {
		WebUI.click(findTestObject('Page_Pembayaran/Icon_Dropdown Transfer VA'))
		WebUI.click(findTestObject('Page_Pembayaran/Icon_Dropdown Opsi Pembayaran'))
		WebUI.click(findTestObject('Page_Pembayaran/Txt_BCA Virtual Account'))
		WebUI.click(findTestObject('Page_Pembayaran/Button_Pilih'))
		WebUI.delay(5)
	}
}