package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class SearchProduct {
	@Given("User on homepage")
	public void user_on_homepage() {
		WebUI.verifyElementVisible(findTestObject('Page_Homepage/txt_Kategori Pilihan'))
	}

	@When("User input value of searchbox with {string}")
	public void user_input_value_of_searchbox_with(String search) {
		WebUI.setText(findTestObject('Page_Homepage/Form_Searchbox'), search)
		WebUI.delay(5)
	}


	@When("User click search result")
	public void user_click_search_result() {
		WebUI.click(findTestObject('Page_Homepage/Txt_Label Stiker Kemasan Satuan (Die Cut)'))
	}

	@Then("User directly to product detail page")
	public void user_directly_to_product_detail_page() {
		WebUI.delay(5)
		WebUI.click(findTestObject('Page_Product Detail/Button_Skip'))
		WebUI.verifyElementVisible(findTestObject('Page_Product Detail/Txt_Label Stiker Kemasan Satuan (Die Cut)'))
		WebUI.delay(5)
	}
}