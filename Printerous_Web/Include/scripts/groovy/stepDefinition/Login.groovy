package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Login {
	@Given("User open URL printerous web")
	public void user_open_URL_printerous_web() {
		WebUI.openBrowser('https://orion.printerous.com/id/')
		WebUI.maximizeWindow()
		WebUI.verifyElementVisible(findTestObject('Page_Homepage/txt_Kategori Pilihan'))
		WebUI.verifyElementVisible(findTestObject('Page_Homepage/txt_Produk Populer'))
	}

	@When("User click button of Masuk in Home Page")
	public void user_click_button_of_Masuk_in_Home_Page() {
		WebUI.click(findTestObject('Page_Homepage/Button_Masuk'))
	}

	@When("User redirect to Login Page")
	public void user_redirect_to_Login_Page() {
		WebUI.verifyElementVisible(findTestObject('Page_Login/Form_Email'))
		WebUI.verifyElementVisible(findTestObject('Page_Login/Form_Password'))
	}

	@When("User input value of email with {string}")
	public void user_input_value_of_email_with(String email) {
		WebUI.setText(findTestObject('Page_Login/Form_Email'), email)
	}

	@When("User input value of password with {string}")
	public void user_input_value_of_password_with(String password) {
		WebUI.setText(findTestObject('Page_Login/Form_Password'), password)
	}

	@When("User click button of Masuk on Login page")
	public void user_click_button_of_Masuk_on_Login_page() {
		WebUI.click(findTestObject('Page_Login/Button_Masuk'))
	}

	@Then("User successfully login")
	public void user_successfully_login() {
		WebUI.verifyElementVisible(findTestObject('Page_Homepage/txt_Kategori Pilihan'))
		WebUI.verifyElementVisible(findTestObject('Page_Homepage/txt_Produk Populer'))
		WebUI.delay(5)
	}
}