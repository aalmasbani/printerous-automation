package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class AddProduct {
	@When("User click button of Pilih Template")
	public void user_click_button_of_Pilih_Template() {
		WebUI.delay(5)
		WebUI.click(findTestObject('Page_Product Detail/Button_Pilih Template'))
	}

	@When("User directly to product template page")
	public void user_directly_to_product_template_page() {
		WebUI.verifyElementVisible(findTestObject('Page_Template/Card_Albondiga'))
	}

	@And("User select template")
	public void user_select_template() {
		WebUI.click(findTestObject('Page_Template/Card_Albondiga'))
	}

	@And("User input value of kuantitas product with {string}")
	public void user_input_value_of_kuantitas_product_with(String kuantitas) {
		WebUI.setText(findTestObject('Page_Template/Form_Kuantitas'), kuantitas)
	}

	@And("User click button of Personalisasi Desain")
	public void user_click_button_of_personalisasi_desain() {
		WebUI.click(findTestObject('Page_Template/Button_Personalisasi Desain'))
		WebUI.delay(5)
	}
}
