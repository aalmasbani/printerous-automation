Feature: SearchProduct

	Background: User want to login using correct credential
	  Given User open URL printerous web
    When User click button of Masuk in Home Page
    And User redirect to Login Page
    And User input value of email with 'testingbbb31@gmail.com'
    And User input value of password with 'Admin1234!'
    And User click button of Masuk on Login page
    Then User successfully login
	
  @SP001
  Scenario: User want to search product on Printerous
    Given User on homepage
    When User input value of searchbox with 'Label Stiker Kemasan Satuan (Die Cut)'
    And User click search result
    Then User directly to product detail page