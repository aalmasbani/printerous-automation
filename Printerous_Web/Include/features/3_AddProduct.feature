Feature: Add Product

	Background: User want to login using correct credential
	  Given User open URL printerous web
    When User click button of Masuk in Home Page
    And User redirect to Login Page
    And User input value of email with 'testingbbb31@gmail.com'
    And User input value of password with 'Admin1234!'
    And User click button of Masuk on Login page
    Then User successfully login
	
  @AP001
  Scenario: User want to search product on Printerous
    Given User on homepage
    When User input value of searchbox with 'Label Stiker Kemasan Satuan (Die Cut)'
    And User click search result
    And User directly to product detail page
    And User click button of Pilih Template
    And User directly to product template page
    And User select template
    And User input value of kuantitas product with '100'
    And User click button of Personalisasi Desain